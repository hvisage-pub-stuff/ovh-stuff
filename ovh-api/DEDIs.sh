#!/bin/zsh
# set -x

source `dirname $0`/functions.sh
source `dirname $0`/secrets.sh


# for i in `jq -r .'[]' /tmp/list-ips.json`
# do
# 	echo $i >&2
# 	apiovh get /ip/`urlencode $i` 
# done > /tmp/list-ips-info.json

`dirname $0`/post-query.sh GET /dedicated/server | tee /tmp/list-dedis.json \
| jq -r '.data[]|@uri' \
| parallel -j 20 `dirname $0`/post-query.sh GET /dedicated/server/{} \
| tee /tmp/list-dedis-info.json \
| jq -r '.req'  \
| parallel -j 20 `dirname $0`/post-query.sh GET \{\}/ips \
| tee /tmp/list-dedis-ips.json

for i in specifications/hardware specifications/ip specifications/network features/backupFTP features/backupFTP/authorizableBlocks option orderable/bandwidthvRack serviceInfos vrack
do
	fn=`echo $i | sed 's|/|_|g'`
	jq -r '.req' /tmp/list-dedis-info.json \
	| parallel -j 20 `dirname $0`/post-query.sh GET {}/$i \
	|tee /tmp/list-dedis-${fn}.json
done

jq -r '.req' /tmp/list-dedis-info.json \
	| parallel -j 20 `dirname $0`/post-query.sh GET {}/virtualMac \
	| tee /tmp/list-dedis-virtualMac.json \
	| jq -rc '.|select((.data|length) >0)| {"req":.req,"array":.data} ' \
	| jq -r '.req as $test | .array|map($test+"/"+.)|.[]'  \
	| parallel -j 50 `dirname $0`/post-query.sh GET \{\} \
	| tee /tmp/list-dedis-vMac-info.json \
	| jq -r ".req" \
	| parallel -j 50 `dirname $0`/post-query.sh GET \{\}/virtualAddress \
	| tee /tmp/list-dedis-vMac-vAddr.json \
	| jq -rc '.|select((.data|length) >0)| {"req":.req,"array":.data} ' \
	| jq -r '.req as $test | .array|map($test+"/"+.)|.[]'  \
	| parallel -j 50 `dirname $0`/post-query.sh GET \{\} \
	| tee /tmp/list-dedis-vMac-vAddr-info.json

jq -r '.req' /tmp/list-dedis-info.json \
	| parallel -j 20 `dirname $0`/post-query.sh GET {}/networkInterfaceController \
	| tee /tmp/list-dedis-NICs.json \
	| jq -rc '.|select((.data|length) >0)| {"req":.req,"array":.data} ' \
	| jq -r '.req as $test | .array|map($test+"/"+.)|.[]'  \
	| parallel -j 20 `dirname $0`/post-query.sh GET {} \
	| tee /tmp/list-dedis-NIC-info.json 

jq -r '.req' /tmp/list-dedis-info.json \
	| parallel -j 20 `dirname $0`/post-query.sh GET {}/virtualNetworkInterface  enabled=true \
	| tee /tmp/list-dedis-vNICs.json \
	| jq -rc '.|select((.data|length) >0)| {"req":.req,"array":.data} ' \
	| jq -r '.req as $test | .array|map($test+"/"+.)|.[]'  \
	| parallel -j 50 `dirname $0`/post-query.sh GET \{\} \
	| tee /tmp/list-dedis-vNIC-info.json

# jq --raw-output \
# '"\""+.routedTo.serviceName+"\" -> \""+ 
#  (if .type == "dedicated" or .type == "vps" 
#   or .type == "cloud" then "d:" 
#  elif .type=="failover" then "" 
#  elif .type == "vrack" then "v:" 
#  else .type+"%" end )+.ip+"\""' \
#   /tmp/list-ips-info.json \
# | grep -v d:

jq  '.data|{(.name):.}' /tmp/list-dedis-info.json \
| jq -s add | tee /tmp/list-dedis-info-kv.json

jq -rc '[.data.name,.data|tostring]|@csv' /tmp/list-dedis-info.json \
 | tee /tmp/list-dedis-info.csv
