#!/usr/bin/env node
//#Original using keyboard sequences

const puppeteer = require('puppeteer');
var myArgs = process.argv.slice(2);
console.log('myArgs: ', myArgs);

console.log(myArgs[0]);

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

	//Let's block images:
	await page.setRequestInterception(true);
	page.on('request', (request) => {
		console.log('requesting:')
		console.log(request.url());
	//    if (request.resourceType() === 'image') request.abort();
		if (request.isNavigationRequest() == false ) request.abort();
		else request.continue();
    });
	page.on('load', 
		(load) => { console.log('page loading'); console.log( load);
		}
	);
  await page.goto(myArgs[0],{
    	waitUntil: 'networkidle2',
  	});
  // console.log(page.url());
  await page.screenshot({path: 'api-start.png'});

  await page.keyboard.type(ovhc.email,2);
  await page.keyboard.press('Tab',1);
  await page.keyboard.type(ovhc.pass,2);
  await page.keyboard.press('Tab',1);
  await page.keyboard.press('ArrowUp',1);
  await page.keyboard.press('ArrowUp',1);
  await page.keyboard.press('Tab',1);
	
  await Promise.all([
	await page.keyboard.press('Enter',1),
	//      page.click("button[type=submit]"),
      page.waitForNavigation({ waitUntil: 'networkidle0' }),
  ]);
	page.once('load', () => console.log('Page loaded!'));
    //await page.waitForNavigation();
    console.log('Finished: '+page.url());
    await page.screenshot({path: 'api.png'});
   await browser.close();
})();