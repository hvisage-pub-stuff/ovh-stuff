#!/bin/zsh
#set -x
export NODE_EXTRA_CA_CERTS=/etc/ssl/certs/ca-certificates.crt

source `dirname $0`/secrets.sh

curl -XPOST -H"X-Ovh-Application: $AK" -H "Content-type: application/json" \
https://ca.api.ovh.com/1.0/auth/credential  -d '{
    "accessRules": [
        {
            "method": "GET",
            "path": "/*"
        }
    ],
    "redirection":"'$REDIR'"
}'  2> /dev/null |tee $ATK | jq .

        # ,
        # {
        #     "method": "PUT",
        #     "path": "/*"
        # }
        # ,
        # {
        #     "method": "POST",
        #     "path": "/*"
        # }
        # ,
        # {
        #     "method": "DELETE",
        #     "path": "/*"
        # }

$ES `jq -r .validationUrl $ATK` $1


