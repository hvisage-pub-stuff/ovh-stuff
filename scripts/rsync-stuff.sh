#!/bin/bash

date
echo $*
#rsync -vrP ovhswift.bofh.one:ovh-stuff/ .  --update --exclude '.git'

rsync -vrP secrets/ ovhswift.bofh.one:ovh-stuff/secrets/ --delete --update 

rsync -vaP . ovhswift.bofh.one:ovh-stuff/ --delete --exclude 'tmp_secrets' --exclude 'secrets'
date
echo "synced"
