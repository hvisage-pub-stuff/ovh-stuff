#!/bin/zsh
#set -x

#info from https://ca.api.ovh.com/createApp/

source `dirname $0`/secrets.sh
source `dirname $0`/functions.sh


#TSTAMP=`curl https://ca.api.ovh.com/1.0/auth/time 2> /dev/null`
## METHOD=$1
## SITE=https://ca.api.ovh.com/1.0
## if [ 'x'$1 = 'x' ]
## then
##  QUERY=/me/bill/
## else
##  QUERY=$2
## fi

## QS=${SITE}$QUERY

## BODY=''
## shakey=`echo -n $AS"+"$CK"+"$METHOD"+"$QS"+"$BODY"+"$TSTAMP|openssl sha1`

## curl -s -H 'X-Ovh-Application:'$AK                   \
## -H 'X-Ovh-Timestamp:'$TSTAMP                                  \
## -H 'X-Ovh-Signature:$1$'$shakey \
## -H 'X-Ovh-Consumer:'$CK             \
## $QS


METHOD=$1
if [ 'x'$2 = 'x' ]
then
 QUERY=/auth/currentCredential
else
 QUERY=$2
fi
BODY=$3



apiovh $METHOD $QUERY $BODY \
| jq -c '{"req":"'$QUERY'","method":"'$METHOD'","body":"'$BODY'","data":.}'
