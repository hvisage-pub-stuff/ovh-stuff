with cte as
 (select req,(data ->> 'ip')::cidr as net,
 	 data->>'type' as type,
 	 data->'routedTo'->>'serviceName' as routeto
 	  from req_data
 	    where
 	      req like '/ip/%' and  not req like '/ip/service%'
 	       and jsonb_path_exists(data,'$.routedTo.serviceName')
 select *,family(net) from cte;




 with ips as
 (select req,(data ->> 'ip')::cidr as net,
      substring(data->>'type',1,2) as type,
      (data->'routedTo'->'serviceName') as routeto
       from req_data
         where
           req like '/ip/%'
          and jsonb_path_exists(data,'$.routedTo.serviceName') )
 select net,routeto,family(net),substring(type,1,1) from ips;


