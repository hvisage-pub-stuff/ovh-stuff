#Function(s) for OVH API

urlencodepipe() {
  local LANG=C; local c; while IFS= read -r c; do
    case $c in [a-zA-Z0-9.~_-]) printf "$c"; continue ;; esac
    printf "$c" | od -An -tx1 | tr ' ' % | tr -d '\n'
  done <<EOF
$(fold -w1)
EOF
  echo
}

#jq -sRr @uri <- 'nother option/method
urlencode() { printf "$*" | urlencodepipe ;}

function apiovh() {
m=`echo $1 | tr a-z A-Z`
case $m in
	INFO| GET| PUT| POST| DELETE)
		METHOD=$m
		;;
	*)
		echo "must specify the HTTP METHOD" 
		exit 123
		;;
esac

QUERY=$2
BODY=$3

TSTAMP=`curl https://ca.api.ovh.com/1.0/auth/time 2> /dev/null`
SITE=https://ca.api.ovh.com/1.0
        QS=${SITE}$QUERY

shakey=`echo -n $AS"+"$CK"+"$METHOD"+"$QS"+"$BODY"+"$TSTAMP|openssl sha1  -binary | xxd -p`

#-w "%{http_code}" \
curl -X $METHOD -s -H 'X-Ovh-Application:'$AK                   \
-H 'Accept: Application/json' \
-H 'Content-Type: application/json;charset=utf-8' \
-H 'X-Ovh-Consumer:'$CK             \
-H 'X-Ovh-Signature:$1$'$shakey \
-H 'X-Ovh-Timestamp:'$TSTAMP                                  \
--data "$BODY" \
--show-error --fail-early $QS
#echo $?
}

