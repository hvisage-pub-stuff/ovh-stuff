export NODE_EXTRA_CA_CERTS=/etc/ssl/certs/ca-certificates.crt
mkdir -p `dirname $0`/../tmp_secrets

SECRETS=`dirname $0`/../secrets/Secrets.json
ATK=`dirname $0`/../tmp_secrets/atk.json

AK=`jq -r .AppKey $SECRETS`
AS=`jq -r .AppSecret $SECRETS`
CK=`jq -r .consumerKey $ATK`
REDIR=`jq -r .Redirect $SECRETS`

ES=`dirname $0`/../nj/e_2.js
