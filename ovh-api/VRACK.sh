#!/bin/zsh
#set -x

source `dirname $0`/functions.sh
source `dirname $0`/secrets.sh


BASE=vrack
`dirname $0`/post-query.sh GET /${BASE} | tee /tmp/list-${BASE}s.json \
| jq -r '.data[]'  \
| parallel -j 20 `dirname $0`/post-query.sh GET /${BASE}/\{\} \
| tee /tmp/list-${BASE}-info.json 

for i in allowedServices serviceInfos 
do
	echo;echo "getting : $i"
	jq -r '.data[]|@uri' /tmp/list-${BASE}s.json \
	| parallel -j 20 `dirname $0`/post-query.sh GET /${BASE}/{}/$i \
	| tee /tmp/list-${BASE}-${i}.json 
	echo ; echo -- "---"; echo
done 


for SUB in ip dedicatedServerInterface ipLoadbalancing legacyVrack dedicatedServer dedicatedCloud cloudProject 
do
	echo;echo "getting : $SUB"
	jq -r '.req' /tmp/list-${BASE}-info.json \
    | parallel -j 20 `dirname $0`/post-query.sh GET \{\}"/${SUB}" \
    | tee /tmp/list-${BASE}-${SUB}s.json \
    | jq -rc '.|select( (.data|length) >0)| {"req":.req,"array":.data} ' \
    | jq -r '.req as $test | .array|map($test+"/"+(.|@uri))|.[]'  \
    | parallel -j 50 `dirname $0`/post-query.sh GET \{\} \
	| tee /tmp/list-${BASE}-${SUB}-info.json
	echo ; echo -- "---"; echo
done


BASE="${BASE}-ip"
for i in  availableZone 
do
	echo;echo "getting : $BASE $i"
	jq -r '.req' /tmp/list-${BASE}-info.json \
	| parallel -j 20 `dirname $0`/post-query.sh GET {}/$i \
	| tee /tmp/list-${BASE}-${i}.json 
	echo ; echo -- "---"; echo
done 
