*** push and query from POstgresql

- https://www.postgresqltutorial.com/postgresql-joins/

RIght, my "size" is still small so a simple PostgreSQL instance will work easily

process (for now):

1) create the json files with the layout `{"req":"/request/URL","data": "{"response":"from OVH"}`

2) change into CSV (all files together) with `jq -rc '[.data.name,.data|tostring]|@csv' > /tmp/kv.csv`

3) copy into postgresql
 i) first into temp table
 ii) then insert into `req_data` with upsert to replace duplicates on request string

4) create views like:

```sql
drop view vnics;

create view vnics 	
  as select (string_to_array(req,'/'))[4] as server,
   data -> 'linkType' as type,
   data -> 'virtualNetworkInterface' as vnic ,
   data -> 'mac' as mac
    from req_data
     where req like '/dedicated/server/%/networkInterfaceController/%';

drop view revips;
create view revIPs
 as select  
 	  urldecode((string_to_array(req,'/'))[3]) ::inet as netblock,
 	 (data ->> 'ipReverse')::inet  as ip,
 	 data ->> 'reverse' as reverse,
 	 family( ( data ->> 'ipReverse' )::inet ) as family
 	  from req_data where req like '/ip/%/reverse/%';

select * from vnics;
```

```sql
select req, (data->>'ip')::cidr as net  from req_data where req LIKE '/ip/%' and array_length((string_to_array(req,'/')),1) =3 and jsonb_path_exists(data,'$.ip') ;


drop view ips;
create or replace view ips as
with ips as (
 	select req,(data ->> 'ip')::cidr as net,
      substring(data->>'type',1,2) as type,
      (data->'routedTo'->'serviceName') as routeto
       from req_data
         where
           req like '/ip/%'
          and jsonb_path_exists(data,'$.routedTo.serviceName') )
 select net,routeto,family(net),substring(type,1,1) as type from ips;

 select routeto,type,abbrev(netblock) as net,ip,reverse from ips join revips on net = netblock  where ips.family=4 order by routeto,type,net,ip ;


```


```sql

select (string_to_array(replace(replace(replace(replace(req,'virtual','v'),'NetworkInterface','NIC'),'networkInterfaceController','NIC'),'virtualAddress','vAddr'),'/'))[4:],jsonb_pretty(data)
 from req_data
  where req like '/dedi%'
   and not req like '%/backupFTP%'
    and jsonb_path_exists(data,'$.virtualMachineName')
 order by req;

create view DEDIvmacs as
 with vmaccte as (
   select req,data,string_to_array(req,'/') as req_array 
   , (data ->> 'ipAddress' )::inet as ip
   , (data ->> 'virtualMachineName') as VMname
   from req_data
    where req like '/dedicated/%' 
     and jsonb_path_exists(data,'$.ipAddress')
 )
 select req_array[4] as dediname
   , req_array[6]::macaddr as vmac
   , ip, VMname
    from vmaccte order by req;

create view dedi_ips as
 with dediraw as
  (select data
   from req_data
    where req like '/dedi%'
     and jsonb_path_exists(data,'$.ip')
     and jsonb_path_exists(data,'$.datacenter')
   ) select 
 (data->>'ip')::inet as ip,
  data->>'name' as dediname ,
  data->>'reverse' as reverse ,
  data->>'linkSpeed' as linkSpeed ,
  data ->>'datacenter' as dc from dediraw;

```




select * from dedi_ips limit 1;
      ip      |         dediname          |           reverse           | linkspeed |  dc
--------------+---------------------------+-----------------------------+-----------+------
 137.74.92.17 | ns3051886.ip-137-74-92.eu | web01-ovh.aquacheckweb.com. | 1000      | gra1
(1 row)

select * from dedivmacs limit 1;
         dediname          |       vmac        |       ip       |   vmname
---------------------------+-------------------+----------------+-------------
 ns3161869.ip-51-91-107.eu | 02:00:00:4b:6d:ee | 51.254.245.201 | deb-desktop
(1 row)

select * from ips limit 1;
       net       |          routeto          | family | type
-----------------+---------------------------+--------+------
 51.178.20.58/32 | ns3162965.ip-51-178-20.eu |      4 | d
(1 row)

select * from revips limit 1;
     netblock      |       ip       |       reverse        | family
-------------------+----------------+----------------------+--------
 178.32.145.240/28 | 178.32.145.241 | tracs01.pscloud.ovh. |      4
(1 row)

select * from vnics limit 1;
           server           |   type   |                  vnic                  |         mac
----------------------------+----------+----------------------------------------+---------------------
 ns31164145.ip-51-91-221.eu | "public" | "93ac7d14-7da1-489b-bbb5-96bbf8b19b8d" | "ac:1f:6b:d2:66:78"
(1 row)


select distinct  dedi_ips.reverse,COALESCE(routeto,dedivmacs.dediname) as server,COALESCE(ips.net,revips.netblock) as net ,COALESCE(dedivmacs.ip,revips.ip) as ip,vmac,vmname from dedi_ips
     FULL JOIN ips ON dedi_ips.dediname = ips.routeto
     FULL JOIN revips ON ips.net::inet = revips.netblock
     FULL JOIN dedivmacs ON revips.ip = dedivmacs.ip order by server,net;



 select distinct
  dedi_ips.reverse servername ,
  routeto,
  ips.family ,
  COALESCE (netblock,net) as netblock ,
  COALESCE (revips.ip) as ip,
  revips.reverse as reverse
 from
  dedi_ips
   full outer join ips on  dediname = routeto
   full outer join revips on net = netblock
   order by routeto
;



select * from
 ips
 inner join dedi_ips on  ips.routeto=dedi_ips.dediname 
 ;






echo 'select distinct
  dedi_ips.reverse ,
  routeto,
  ips.family,
  netblock,
  revips.ip,
  revips.reverse
 from
  dedi_ips
   right join ips on  dediname = routeto
   join revips on net = netblock
   left join dedivmacs on revips.ip = dedivmacs.ip
order by
 routeto,
 netblock,
 revips.ip ' | psql


drop view server_ips;
create view server_ips as select distinct
  dedi_ips.reverse servername ,
  routeto,
  ips.family ,
  netblock,
  revips.ip as ip,
  revips.reverse as reverse,
  vmac
 from
  dedi_ips
   full outer join ips on  dediname = routeto
   full outer join revips on net = netblock
   full outer  join dedivmacs on revips.ip = dedivmacs.ip
;



select distinct
  dedi_ips.reverse servername ,
  routeto,
  ips.family ,
  COALESCE (netblock,net) as netblock ,
  COALESCE (revips.ip,dedivmacs.ip) as ip,
  revips.reverse as reverse,
  vmac, vmname
 from
    dedivmacs 
     left outer join  dedi_ips on  dedivmacs.dediname=dedi_ips.dediname
    right join ips on  dedi_ips.dediname = routeto
    left join revips on net = netblock
   order by routeto,netblock,ip
;




    join dedivmacs on dedivmacs.dediname=dedi_ips.dediname


copy (SELECT DISTINCT dedi_ips.reverse AS servername,
    ips.routeto,
    revips.netblock,
    COALESCE(revips.ip,dedivmacs.ip),
    revips.reverse,
    dedivmacs.vmac,vmname,ips.type
   FROM dedi_ips
     FULL JOIN ips ON dedi_ips.dediname = ips.routeto
     FULL JOIN revips ON ips.net::inet = revips.netblock
     FULL JOIN dedivmacs ON revips.ip = dedivmacs.ip
    where ips.family = 4 and ips.type in ('f','v') ) to '/tmp/hvt.csv' (format csv,header);









