#!/bin/zsh
#
#set -x
#
# List cloud projects and their IPs & instances
#
source `dirname $0`/functions.sh
source `dirname $0`/secrets.sh



PQ=`dirname $0`/post-query.sh


list=`$PQ GET /cloud/project | jq -r '.data[]'`
echo $list

read TEST
for i in `echo $list`
do
	$PQ GET /cloud/project/$i | jq .
	$PQ GET /cloud/project/$i/ip | jq .
	# apiovh /cloud/project/$i/instance | jq .
done
