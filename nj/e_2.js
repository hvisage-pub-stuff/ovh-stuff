#!/usr/bin/node --unhandled-rejections=strict

const homedir = require('os').homedir();
const ovhc=require(homedir+'/.Config.json');
const puppeteer = require('puppeteer');
var myArgs = process.argv.slice(2);
console.log('myArgs: ', myArgs);

console.log(myArgs[0]);

var ValidityTime=toString(300)
if (myArgs[1] > 1) ValidityTime=toString(myArgs[1]);

(async () => {
  const browser = await puppeteer.launch(
   // { dumpio: true }
    );
  const page = await browser.newPage();
  const timeout = 15000;
  page.setDefaultTimeout(timeout);

	//Let's block images:
  //*** lets not faster things
	await page.setRequestInterception(true);
	page.on('request', (request) => {
		//console.log('requesting:'+request.url()+' ->'+(request.resourceType() === 'image'));
	  // if (request.resourceType() === 'image') request.abort();
		if (request.isNavigationRequest() == false ) request.abort();
		else request.continue();
    });
	page.on('load',
		(load) => { console.log('page loading ');
		}
	);
  //***


    {
        const targetPage = page;
        const promises = [];
        promises.push(targetPage.waitForNavigation());
    //    await targetPage.goto("chrome://new-tab-page/");
    await targetPage.goto(myArgs[0]);
    //,{
    //  waitUntil: 'networkidle2',
    //});
        await Promise.all(promises);
    }

  // console.log(page.url());
  await page.screenshot({path: '/tmp/api-start.png'});
  
  // await page.focus('input[placeholder="#account"]');
  // await page.keyboard.type(ovhc.Email,2);
 {
        const targetPage = page;
        const element = await waitForSelectors([["aria/Account ID or email address"],["#account"]], targetPage, { timeout, visible: true });
        // await scrollIntoViewIfNeeded(element, timeout);
        const type = await element.evaluate(el => el.type);
        // if (["select-one"].includes(type)) {
        //   await element.select("accountid-ovh");
        // } else if (["textarea","text","url","tel","search","password","number","email"].includes(type)) {
          await element.type(ovhc.Email);
        // } else {
        //   await element.focus();
        //   await element.evaluate((el, value) => {
        //     el.value = value;
        //     el.dispatchEvent(new Event('input', { bubbles: true }));
        //     el.dispatchEvent(new Event('change', { bubbles: true }));
        //   }, "accountid-ovh");
        // }
    }
      await page.screenshot({path: '/tmp/api-start-account.png'});

  // page.focus('input[placeholder="#password"]');
//  await page.keyboard.type(ovhc.Pass,2);
{
        const targetPage = page;
        const element = await waitForSelectors([["aria/Password"],["#password"]], targetPage, { timeout, visible: true });
        // await scrollIntoViewIfNeeded(element, timeout);
        const type = await element.evaluate(el => el.type);
        // if (["select-one"].includes(type)) {
        //   await element.select("AcountPasswordHere");
        // } else if (["textarea","text","url","tel","search","password","number","email"].includes(type)) {
          await element.type(ovhc.Pass);
        // } else {
        //   await element.focus();
        //   await element.evaluate((el, value) => {
        //     el.value = value;
        //     el.dispatchEvent(new Event('input', { bubbles: true }));
        //     el.dispatchEvent(new Event('change', { bubbles: true }));
        //   }, "AcountPasswordHere");
        // }
    }
  //await page.select('#selectValidity', ValidityTime);

  await page.screenshot({path: '/tmp/api-pre.png'});
  //await page.click('button[type=submit]');
 //  await Promise.all([
	// // await page.keyboard.press('Enter',1),
	//      page.click("button[type=#login-submit]"),
 //      page.waitForNavigation({ waitUntil: 'networkidle2' }),
 //  ]);
     console.log('push button: '+page.url());

{
        const targetPage = page;
        const promises = [];
        promises.push(targetPage.waitForNavigation());
        const element = await waitForSelectors([["aria/Login"],["#login-submit"]], targetPage, { timeout, visible: true });
        // await scrollIntoViewIfNeeded(element, timeout);
        await element.click({
          offset: {
            x: 61.45135498046875,
            y: 39.68402099609375,
          },
        });
        await Promise.all(promises);
    }

    // await page.waitForNavigation();
    {
        const targetPage = page;
        const promises = [];
        promises.push(targetPage.waitForNavigation());
        const element = await waitForSelectors([["aria/Authorize"],["#apiv6-authorize-submit"]], targetPage, { timeout, visible: true });
        // await scrollIntoViewIfNeeded(element, timeout);
        await element.click({
          offset: {
            x: 94.44439697265625,
            y: 19.683990478515625,
          },
        });
        await Promise.all(promises);
    }
    console.log('Finished: '+page.url());
    await page.screenshot({path: '/tmp/api.png'});
   await browser.close();

   async function waitForSelectors(selectors, frame, options) {
      for (const selector of selectors) {
        try {
          return await waitForSelector(selector, frame, options);
        } catch (err) {
          console.error(err);
        }
      }
      throw new Error('Could not find element for selectors: ' + JSON.stringify(selectors));
    }

    async function scrollIntoViewIfNeeded(element, timeout) {
      await waitForConnected(element, timeout);
      const isInViewport = await element.isIntersectingViewport({threshold: 0});
      if (isInViewport) {
        return;
      }
      await element.evaluate(element => {
        element.scrollIntoView({
          block: 'center',
          inline: 'center',
          behavior: 'auto',
        });
      });
      await waitForInViewport(element, timeout);
    }

    async function waitForConnected(element, timeout) {
      await waitForFunction(async () => {
        return await element.getProperty('isConnected');
      }, timeout);
    }

    async function waitForInViewport(element, timeout) {
      await waitForFunction(async () => {
        return await element.isIntersectingViewport({threshold: 0});
      }, timeout);
    }

    async function waitForSelector(selector, frame, options) {
      if (!Array.isArray(selector)) {
        selector = [selector];
      }
      if (!selector.length) {
        throw new Error('Empty selector provided to waitForSelector');
      }
      let element = null;
      for (let i = 0; i < selector.length; i++) {
        const part = selector[i];
        if (element) {
          element = await element.waitForSelector(part, options);
        } else {
          element = await frame.waitForSelector(part, options);
        }
        if (!element) {
          throw new Error('Could not find element: ' + selector.join('>>'));
        }
        if (i < selector.length - 1) {
          element = (await element.evaluateHandle(el => el.shadowRoot ? el.shadowRoot : el)).asElement();
        }
      }
      if (!element) {
        throw new Error('Could not find element: ' + selector.join('|'));
      }
      return element;
    }

    async function waitForElement(step, frame, timeout) {
      const count = step.count || 1;
      const operator = step.operator || '>=';
      const comp = {
        '==': (a, b) => a === b,
        '>=': (a, b) => a >= b,
        '<=': (a, b) => a <= b,
      };
      const compFn = comp[operator];
      await waitForFunction(async () => {
        const elements = await querySelectorsAll(step.selectors, frame);
        return compFn(elements.length, count);
      }, timeout);
    }

    async function querySelectorsAll(selectors, frame) {
      for (const selector of selectors) {
        const result = await querySelectorAll(selector, frame);
        if (result.length) {
          return result;
        }
      }
      return [];
    }

    async function querySelectorAll(selector, frame) {
      if (!Array.isArray(selector)) {
        selector = [selector];
      }
      if (!selector.length) {
        throw new Error('Empty selector provided to querySelectorAll');
      }
      let elements = [];
      for (let i = 0; i < selector.length; i++) {
        const part = selector[i];
        if (i === 0) {
          elements = await frame.$$(part);
        } else {
          const tmpElements = elements;
          elements = [];
          for (const el of tmpElements) {
            elements.push(...(await el.$$(part)));
          }
        }
        if (elements.length === 0) {
          return [];
        }
        if (i < selector.length - 1) {
          const tmpElements = [];
          for (const el of elements) {
            const newEl = (await el.evaluateHandle(el => el.shadowRoot ? el.shadowRoot : el)).asElement();
            if (newEl) {
              tmpElements.push(newEl);
            }
          }
          elements = tmpElements;
        }
      }
      return elements;
    }

    async function waitForFunction(fn, timeout) {
      let isActive = true;
      setTimeout(() => {
        isActive = false;
      }, timeout);
      while (isActive) {
        const result = await fn();
        if (result) {
          return;
        }
        await new Promise(resolve => setTimeout(resolve, 100));
      }
      throw new Error('Timed out');
    }
})();
