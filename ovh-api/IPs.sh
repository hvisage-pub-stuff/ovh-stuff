#!/bin/zsh
# set -x

source `dirname $0`/functions.sh
source `dirname $0`/secrets.sh


echo "Fetching list, and reverse first pass:"


`dirname $0`/post-query.sh GET /ip | tee /tmp/list-ips.json \
| jq -r '.data[]|@uri'  \
| parallel -j 20 `dirname $0`/post-query.sh GET /ip/{} \
| tee /tmp/list-ips-info.json \
| jq -r '.data.ip|@uri'  \
| parallel -j 20 `dirname $0`/post-query.sh GET /ip/{}/reverse \
| tee /tmp/list-ips-reverse.json \
| jq -rc '.|select( (.data|length) >0)| {"req":.req,"array":.data} ' \
| jq -r '.req as $test | .array|map($test+"/"+.)|.[]'  \
| parallel -j 50 ./post-query.sh GET \{\} \
| tee /tmp/list-ips-reverse-info.json \
| jq -r '.data|(.ipReverse+" "+.reverse)' \
| sort -n

echo;echo "Firewalled IPs"

jq -r '.data[]|@uri' /tmp/list-ips.json  \
| parallel -j 20 `dirname $0`/post-query.sh GET /ip/{}/firewall \
| tee /tmp/list-ips-firewall.json \
| jq -rc '.|select((.data|length) >0)| {"req":.req,"array":.data} ' \
| jq -r '.req as $test | .array|map($test+"/"+.)|.[]'  \
| parallel -j 50 `dirname $0`/post-query.sh GET \{\} \
| tee /tmp/list-ips-firewalled.json \
| jq -r '.req' \
| parallel -j 50 `dirname $0`/post-query.sh GET \{\}"/rule" \
| tee /tmp/list-ips-firewall-rule-seqs.json \
| jq -rc '.|select((.data|length) >0)| {"req":.req,"array":.data} ' \
| jq -r '.req as $test | .array|map($test+"/"+(.|tostring))|.[]'  \
| parallel -j 50 `dirname $0`/post-query.sh GET \{\} \
| tee /tmp/list-ips-firewall-rule-seqs-info.json

echo;echo "Spamming IPs"

jq -r '.data[]|@uri' /tmp/list-ips.json  \
| parallel -j 20 `dirname $0`/post-query.sh GET /ip/{}/spam \
| tee /tmp/list-ips-spam.json \
| jq -rc '.|select((.data|length) >0)| {"req":.req,"array":.data} ' \
| jq -r '.req as $test | .array|map($test+"/"+.)|.[]'  \
| parallel -j 50 `dirname $0`/post-query.sh GET \{\} \
| tee /tmp/list-ips-spamming.json 



jq -rc '[.data.ip,.data|tostring]|@csv' /tmp/list-ips-info.json \
| tee /tmp/list-ips-info.csv

for i in ripe  delegation arp phishing move   mitigation mitigationProfiles game  antihack 
do
	echo;echo "getting : $i"
	jq -r '.data[]|@uri' /tmp/list-ips.json \
	| parallel -j 20 `dirname $0`/post-query.sh GET /ip/{}/$i \
	| tee /tmp/list-ips-${i}.json 
	echo ; echo -- "---"; echo
done

jq --raw-output \
'.data|"\""+.routedTo.serviceName+"\" -> \""+ 
 (if .type == "dedicated" or .type == "vps" 
  or .type == "cloud" then "d:" 
 elif .type=="failover" then "" 
 elif .type == "vrack" then "v:" 
 else .type+"%" end )+.ip+"\""' \
  /tmp/list-ips-info.json \
| grep -v d:

`dirname $0`/post-query.sh GET /ip/service | tee /tmp/list-ip-services.json \
| jq -r '.data[]|@uri'  \
| parallel -j 20 `dirname $0`/post-query.sh GET /ip/service/{} \
| tee /tmp/list-ip-services-info.json
jq -r '.data[]|@uri' /tmp/list-ip-services.json \
| parallel -j 20 `dirname $0`/post-query.sh GET /ip/service/{}/serviceInfos \
| tee /tmp/list-ip-services-serviceinfo.json


#jq -rc 'if (.req | test("reverse")) and (.data|length) >0 then {"req":.req,"array":.data} else "" end' *json |uniq | jq -c .array

# jq -rc '.|select((.req | test("reverse")) and (.data|length) >0)| {"req":.req,"array":.data} ' /tmp/*json| jq '.req as $test | .array|map($test+"/"+.)|.[]'

#jq -rc 'if (.req | test("reverse")) and (.data|length) >0 then {"req":.req,"array":.data} else "" end' /tmp/*json 2> /dev/null | jq -r '.req as $test | .array|map($test+"/"+.)|.[]'  | parallel -j 50 ./post-query.sh GET \{\} | tee /tmp/list-ips-reverse-info.json | jq -r '.data|(.ipReverse+" "+.reverse)' | sort -n