#!/usr/bin/env node
const homedir = require('os').homedir();
const ovhc=require(homedir+'/.Config.json');
const puppeteer = require('puppeteer');
var myArgs = process.argv.slice(2);
console.log('myArgs: ', myArgs);

console.log(myArgs[0]);

var ValidityTime=toString(300)
if (myArgs[1] > 1) ValidityTime=toString(myArgs[1]);

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

	//Let's block images:
	await page.setRequestInterception(true);
	page.on('request', (request) => {
		console.log('requesting:'+request.url()+' ->'+(request.resourceType() === 'image'));
	   // if (request.resourceType() === 'image') request.abort();
		if (request.isNavigationRequest() == false ) request.abort();
		else request.continue();
    });
	page.on('load', 
		(load) => { console.log('page loading');
		}
	);
  await page.goto(myArgs[0],{
    	waitUntil: 'networkidle2',
  	});
  // console.log(page.url());
  await page.screenshot({path: '/tmp/api-start.png'});
  
  await page.focus('input[placeholder="Email"]');
  await page.keyboard.type(ovhc.Email,2);
  
  await page.focus('input[placeholder="Password"]');
  await page.keyboard.type(ovhc.Pass,2);

  await page.select('#selectValidity', ValidityTime);

  await page.screenshot({path: '/tmp/api-pre.png'});
  //await page.click('button[type=submit]');
  await Promise.all([
	// await page.keyboard.press('Enter',1),
	     page.click("button[type=submit]"),
      page.waitForNavigation({ waitUntil: 'networkidle2' }),
  ]);
    // await page.waitForNavigation();
    console.log('Finished: '+page.url());
    await page.screenshot({path: '/tmp/api.png'});
   await browser.close();
})();
