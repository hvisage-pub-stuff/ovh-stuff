---

begin transaction;

create temp table tmp_req_data  (like req_data) on commit drop;

copy tmp_req_data from '/tmp/kv.csv' with csv;

insert into req_data
 select distinct on (req,data) * from tmp_req_data
  on conflict on constraint req_data_pkey do update 
	SET data = EXCLUDED.data 
;

select count(*) from req_data;

commit;