#!/bin/zsh
# set -x

source `dirname $0`/functions.sh
source `dirname $0`/secrets.sh

BASE=service
`dirname $0`/post-query.sh GET /${BASE} | tee /tmp/list-${BASE}.json \
| jq -r '.data[]'  \
| parallel -j 20 `dirname $0`/post-query.sh GET /${BASE}/\{\} \
| tee /tmp/list-${BASE}-info.json \
| jq -r '.data.route|.vars[0] as $v | .path|sub( "{"+$v.key + "}"; $v.value|@uri )' \
| parallel -j 20 `dirname $0`/post-query.sh GET \{\} \
| tee /tmp/list-${BASE}-route_url.json \
| jq .

BASE=services
`dirname $0`/post-query.sh GET /${BASE} | tee /tmp/list-${BASE}.json \
| jq -r '.data[]'  \
| parallel -j 20 `dirname $0`/post-query.sh GET /${BASE}/\{\} \
| tee /tmp/list-${BASE}-info.json \
| jq -r '.req' \
| parallel -j 20 `dirname $0`/post-query.sh GET \{\}"/billing/engagement" \
| tee /tmp/list-${BASE}-billing-engage-info.json

cat /tmp/list-${BASE}-info.json \
| jq -r '.req' \
| parallel -j 20 `dirname $0`/post-query.sh GET \{\}"/options" \
| tee /tmp/list-${BASE}-options-info.json

