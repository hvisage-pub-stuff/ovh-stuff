# ovh-stuff

Stuff for accessing/simplify OVH API related stuff

Nothing guaranteed, nothing supported... but would be great if you add your own additions etc. to help expand ;)

Might be specific for me, but use as you see/etc. fit

https://docs.ovh.com/ca/en/customer/first-steps-with-ovh-api/
https://ca.api.ovh.com/createApp/

The file to add your specific keys you fetched from https://ca.api.ovh.com/createApp/
 `secrets/Secrets.json`:

```json
{
    "AppKey": "fromCreateApp",
 "AppSecret": "AlsoFromCreateAppAbove",
  "Redirect": "http://www.example.com/"
}
```

For the automated Authorization to work, you need a file in `$HOME/.Config.json` (The words security issue comes to mind in big red letters), as shown here:
```javascript
const homedir = require('os').homedir();
const ovhc=require(homedir+'/.Config.json');
```
that `$HOME/.Config.json` contains your email/OVH NIC handle (I mentioned security red flags??) and your password (definate problem) example:
```json
{
 "Email": "sp00000-ovh",
  "Pass": "SecretPasswordToOVH"
}
```
(I haven't yet checked this with 2 factor authentication, but would be interesting to test etc.)

The `ovh-api/pre_query.sh` will do the needed (you need a functional `puppeteer` installation) with the above information to create the needed tickets etc.

once that is done, you can use `ovh-api/post-query.sh` or `apiovh.sh` to do queries that uses the function(s) in `ovh-api/functions.sh` 

I've started to fetch data I'll be pushing into POstgreSQL's `JSONB` data structres for querying the needed data etc. from the servers.

Info from all dedicated servers:
`./post-query.sh GET /dedicated/server | jq '.[]'| xargs -tn1 -I  \{\} ./post-query.sh GET /dedicated/server/\{\}`


<<<<<<< HEAD



pvesh get /pools/AfricaDirect --output-format json | jq -r '.members|map({id,node,name,type})[]|"pvesh get /nodes/"+.node+"/"+.id+"/config -output-format json |jq -s '\''map({name,net0})[]|.+{id:\""+.id+"\",node:\""+.node+"\"}'\''"'|sh|jq -r '"pvesh set /nodes/"+.node+"/"+.id+"/config -net0 "+.net0+",link_down=1"'|sh -x

=======
>>>>>>> origin/master
