#!/bin/zsh
# set -x

source `dirname $0`/functions.sh
source `dirname $0`/secrets.sh



PQ=`dirname $0`/post-query.sh

$PQ GET /me > /tmp/me.json
$PQ GET /me/agreements | tee /tmp/me-agreements.json \
 | jq -r '.data[]' | parallel -j 20 ./post-query.sh GET /me/agreements/\{\}/contract > /tmp/me-agreements-contract.json
cat  /tmp/me-agreements.json | jq -r '.data[]' | parallel -j 20 ./post-query.sh GET /me/agreements/\{\} > /tmp/me-agreements-status.json


# Need to re visit when needing the contracts in DB
# for i in me me-agreements  me-agreements-status me-agreements-contract
# do
# 	jq  '.data|{(.name):.}' /tmp/${i}.json \
#  | jq -s add | tee /tmp/${i}-add.json |
# jq -rc '[.data.name,.data|tostring]|@csv' \
#   > /tmp/$i.csv
# done