--
-- PostgreSQL database dump
--

-- Dumped from database version 12.3 (Debian 12.3-1.pgdg100+1)
-- Dumped by pg_dump version 12.3 (Debian 12.3-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'SQL_ASCII';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpython3u; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpython3u WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpython3u; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpython3u IS 'PL/Python3U untrusted procedural language';


--
-- Name: jsonb_plpython3u; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS jsonb_plpython3u WITH SCHEMA public;


--
-- Name: EXTENSION jsonb_plpython3u; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION jsonb_plpython3u IS 'transform between jsonb and plpython3u';


--
-- Name: urldecode(character varying); Type: FUNCTION; Schema: public; Owner: hvisage
--

CREATE FUNCTION public.urldecode(a character varying) RETURNS character varying
    LANGUAGE plpython3u
    AS $$
    from urllib.parse import unquote

    return unquote(a)
$$;


ALTER FUNCTION public.urldecode(a character varying) OWNER TO hvisage;

--
-- Name: urlescape(text); Type: FUNCTION; Schema: public; Owner: hvisage
--

CREATE FUNCTION public.urlescape(original text) RETURNS text
    LANGUAGE plpython3u IMMUTABLE STRICT
    AS $$
import urllib
return urllib.quote(original);
$$;


ALTER FUNCTION public.urlescape(original text) OWNER TO hvisage;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: req_data; Type: TABLE; Schema: public; Owner: hvisage
--

CREATE TABLE public.req_data (
    req character varying NOT NULL,
    data jsonb
);


ALTER TABLE public.req_data OWNER TO hvisage;

--
-- Name: dedi_ips; Type: VIEW; Schema: public; Owner: hvisage
--

CREATE VIEW public.dedi_ips AS
 WITH dediraw AS (
         SELECT req_data.data
           FROM public.req_data
          WHERE (((req_data.req)::text ~~ '/dedi%'::text) AND jsonb_path_exists(req_data.data, '$."ip"'::jsonpath) AND jsonb_path_exists(req_data.data, '$."datacenter"'::jsonpath))
        )
 SELECT ((dediraw.data ->> 'ip'::text))::inet AS ip,
    (dediraw.data ->> 'name'::text) AS dediname,
    (dediraw.data ->> 'reverse'::text) AS reverse,
    (dediraw.data ->> 'linkSpeed'::text) AS linkspeed,
    (dediraw.data ->> 'datacenter'::text) AS dc
   FROM dediraw;


ALTER TABLE public.dedi_ips OWNER TO hvisage;

--
-- Name: dedivmacs; Type: VIEW; Schema: public; Owner: hvisage
--

CREATE VIEW public.dedivmacs AS
 WITH vmaccte AS (
         SELECT req_data.req,
            req_data.data,
            string_to_array((req_data.req)::text, '/'::text) AS req_array,
            ((req_data.data ->> 'ipAddress'::text))::inet AS ip,
            (req_data.data ->> 'virtualMachineName'::text) AS vmname
           FROM public.req_data
          WHERE (((req_data.req)::text ~~ '/dedicated/%'::text) AND jsonb_path_exists(req_data.data, '$."ipAddress"'::jsonpath))
        )
 SELECT vmaccte.req_array[4] AS dediname,
    (vmaccte.req_array[6])::macaddr AS vmac,
    vmaccte.ip,
    vmaccte.vmname
   FROM vmaccte
  ORDER BY vmaccte.req;


ALTER TABLE public.dedivmacs OWNER TO hvisage;

--
-- Name: ips; Type: VIEW; Schema: public; Owner: hvisage
--

CREATE VIEW public.ips AS
 WITH ips AS (
         SELECT req_data.req,
            ((req_data.data ->> 'ip'::text))::cidr AS net,
            "substring"((req_data.data ->> 'type'::text), 1, 2) AS type,
            ((req_data.data -> 'routedTo'::text) -> 'serviceName'::text) AS routeto
           FROM public.req_data
          WHERE (((req_data.req)::text ~~ '/ip/%'::text) AND jsonb_path_exists(req_data.data, '$."routedTo"."serviceName"'::jsonpath))
        )
 SELECT ips.net,
    ips.routeto,
    family((ips.net)::inet) AS family,
    "substring"(ips.type, 1, 1) AS type
   FROM ips;


ALTER TABLE public.ips OWNER TO hvisage;

--
-- Name: revips; Type: VIEW; Schema: public; Owner: hvisage
--

CREATE VIEW public.revips AS
 SELECT (public.urldecode(((string_to_array((req_data.req)::text, '/'::text))[3])::character varying))::inet AS netblock,
    ((req_data.data ->> 'ipReverse'::text))::inet AS ip,
    (req_data.data ->> 'reverse'::text) AS reverse,
    family(((req_data.data ->> 'ipReverse'::text))::inet) AS family
   FROM public.req_data
  WHERE ((req_data.req)::text ~~ '/ip/%/reverse/%'::text);


ALTER TABLE public.revips OWNER TO hvisage;

--
-- Name: vnics; Type: VIEW; Schema: public; Owner: hvisage
--

CREATE VIEW public.vnics AS
 SELECT (string_to_array((req_data.req)::text, '/'::text))[4] AS server,
    (req_data.data -> 'linkType'::text) AS type,
    (req_data.data -> 'virtualNetworkInterface'::text) AS vnic,
    (req_data.data -> 'mac'::text) AS mac
   FROM public.req_data
  WHERE ((req_data.req)::text ~~ '/dedicated/server/%/networkInterfaceController/%'::text);


ALTER TABLE public.vnics OWNER TO hvisage;

--
-- Name: req_data req_data_pkey; Type: CONSTRAINT; Schema: public; Owner: hvisage
--

ALTER TABLE ONLY public.req_data
    ADD CONSTRAINT req_data_pkey PRIMARY KEY (req);


--
-- PostgreSQL database dump complete
--

